<?php

use yii\db\Migration;

class m201226_094727_create_catalog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalog}}', [
            'product_id' => $this->primaryKey(),
            'product_type' => $this->integer()->notNull(),
            'name' => $this->string(),
            'sku' => $this->string(),
            'image' => $this->string(),
            'on_warehouse' => $this->integer(),
        ]);       
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalog}}');
    }
}
