<?php

use yii\db\Migration;

/**
 * Handles the creation of table {{%user}}.
 */
class m201226_100618_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
			'id' => $this->primaryKey(),
			'email' => $this->string(),
			'password' => $this->string(),
			'status' => $this->integer(),
			'username' => $this->string(45),
			'password_hash' => $this->string(),
			'auth_key' => $this->string(),
			'created_at' => $this->integer(),
			'updated_at'  => $this->integer(),
			'password_reset_token' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
