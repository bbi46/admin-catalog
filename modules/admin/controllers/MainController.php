<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use app\modules\admin\models\Catalog;
use app\modules\admin\models\Settings;
use yii\web\Response;

class MainController extends Controller {
	public function actionIndex() {		
		return $this->render('index', [
		]);
	}
}