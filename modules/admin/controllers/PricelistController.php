<?php
namespace app\modules\admin\controllers;

use Yii;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use app\modules\admin\models\Manufacturer;
use app\modules\admin\models\Product;
use app\modules\admin\models\Provider;
use yii\web\UploadedFile;

class PricelistController extends \yii\web\Controller
{	
	public function actionUpload()
    {
		define('UPLOAD_DIR',  Yii::getAlias('@webroot') . '/uploads/');
		$model = new Product();

        if ($model->load(Yii::$app->request->post())) {
			
            $priceFile = UploadedFile::getInstance($model, 'priceFile');
            $model->priceFile = $priceFile;
            $filename = $model->priceFile->baseName . '.' . $model->priceFile->extension;
            $providerByFilename = $model->priceFile->baseName;
			$provider = '';
			
				if(preg_match('/Kosatec/', $providerByFilename)) {
					$provider = 'Kosatec';
				} else if(preg_match('/Systeam Bestandsliste/', $providerByFilename)) {
					$provider = 'Systeam Bestandsliste';
				}
				
				$providerExists = Provider::find()
					->where('name=:provider_name', [':provider_name' => $provider])
					->one()->name;
					
				if(!$providerExists) {
					$providerModel = new Provider;
					$providerModel->name = $provider;
					$providerModel->save();
				}
				
            $model->priceFile->saveAs(UPLOAD_DIR . $filename);
            $uploadedFile = UPLOAD_DIR . $filename;
			  
			$objPHPExcel = IOFactory::load($uploadedFile);
			$rowData = $objPHPExcel->getSheet(0);
			
			$objPHPExcel->getSheet(0);
			
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$highestRow         = $worksheet->getHighestRow();
				$highestColumn      = $worksheet->getHighestColumn();
				$highestColumnIndex = Coordinate::columnIndexFromString($highestColumn);
			}

			echo '<pre>';
			print_r($rowData->rangeToArray('A1:' . $highestColumn . $highestRow));
			echo '</pre>';
			die();
			
			$attributes = $rowData->rangeToArray('A1:'.$highestColumn. '1', null, true, true);
			
			$neededColumnsArr = ['herstnr', 'ean', 'hek'];
			
			$columnsArr = [];
			$productAttributesArr = $attributes[0];
			
			// get provider number column index
			$columnIndex = array_search('herstnr', $productAttributesArr);
			$columnIndex++;
			$columnsArr[] = $columnIndex;
			
			// get provider name column index
			$columnIndex = array_search('hersteller', $productAttributesArr);
			$columnIndex++;
			$columnsArr[] = $columnIndex;
			
			// get art(product) name column index
			foreach(['artname', 'Description'] as $pName) {
				$columnIndex = array_search($pName, $productAttributesArr);
				if($columnIndex) {
					break;
				}
			}
			
			$columnIndex++;
			$columnsArr[] = $columnIndex;
			
			// get product count column index
			$columnIndex = array_search('menge', $productAttributesArr);
			$columnIndex++;
			$columnsArr[] = $columnIndex;
			
			// get product ean column index
			$columnIndex = array_search('ean', $productAttributesArr);
			$columnIndex++;
			$columnsArr[] = $columnIndex;
			
			// get product hek (maybe price) column index
			$columnIndex = array_search('hek', $productAttributesArr);
			$columnIndex++;
			$columnsArr[] = $columnIndex;
			
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {				
				$manufacturerNumCol = $columnsArr[0];
				$manufacturerNameCol = $columnsArr[1];
				$productNameCol = $columnsArr[2];
				$productCountCol = $columnsArr[3];
				$productEanCol = $columnsArr[4];
				$productPriceCol = $columnsArr[5];
				
				for ($row = 1; $row <= $highestRow ; ++$row) {
					$manufacturerNumCell = $worksheet->getCellByColumnAndRow($manufacturerNumCol, $row);
					$manufacturerNameCell = $worksheet->getCellByColumnAndRow($manufacturerNameCol, $row);
					$productNameCell = $worksheet->getCellByColumnAndRow($productNameCol, $row);
					$productCountCell = $worksheet->getCellByColumnAndRow($productCountCol, $row);
					$productEanCell = $worksheet->getCellByColumnAndRow($productEanCol, $row);
					$productPriceCell = $worksheet->getCellByColumnAndRow($productPriceCol, $row);
					
					$manufacturerNumColumnName =  Coordinate::stringFromColumnIndex($manufacturerNumCol);
					$manufacturerNameColumnName =  Coordinate::stringFromColumnIndex($manufacturerNameCol);
					$productNameColumnName =  Coordinate::stringFromColumnIndex($productNameCol);
					$productCountColumnName =  Coordinate::stringFromColumnIndex($productCountCol);
					$productEanColumnName =  Coordinate::stringFromColumnIndex($productEanCol);
					$productPriceColumnName =  Coordinate::stringFromColumnIndex($productPriceCol);
					
					$numVal = $manufacturerNumCell->getValue();
					$nameVal = $manufacturerNameCell->getValue();
					$productNameVal = $productNameCell->getValue();
					$productCountVal = $productCountCell->getValue();
					$productEanVal = $productEanCell->getValue();
					$productPriceVal = $productPriceCell->getValue();
					
					if($row != 1) {
						$manufacturerNumberExists = Manufacturer::find()->where('number=:pnum', [':pnum' => trim($numVal)])->one()->number;
						if(!$manufacturerNumberExists) {
							$manufacturer = new Manufacturer;
							$manufacturer->name = $nameVal;
							$manufacturer->number = $numVal;
							if($nameVal && $numVal !== null) {
								$manufacturer->save(false);
							}
						}
						
						$productNameExists = Product::find()
							->where('ean=:pean', [':pean' => trim($productEanVal)])
							->one()
							->ean;
							
						if(!$productNameExists) {
							$product = new Product;
							$product->name = trim(htmlspecialchars($productNameVal));
							
							$providerId = Provider::find()
								->where('name=:prov_name', [':prov_name' => $provider])
								->one()->id;
							$product->provider_id = $providerId ?? 0;
							
							$manufacturerId = Manufacturer::find()
								->where('number=:m_number', [':m_number' => $numVal])
								->one()->id;
							$product->manufacturer_id = $manufacturerId ?? 0;
							
							$product->count = $productCountVal ?? 0;
							$product->ean = $productEanVal ?? 0;
							$product->price = $productPriceVal ?? 0;
							
							if($productNameVal !== null) {
								$product->save(false);
							}
						}
					}
				}
			if($tI == 1) {
				exit;
			}
	}
		
			return $this->render('pricedata', [
				'attributes' => $attributes,
				'sheet_obj' => $objPHPExcel,
				'provider' => $provider,
				'manufacturerNumColumn' => $manufacturerNumColumnName,
				'manufacturerNameColumn' => $manufacturerNameColumnName
			]);
        } 
			
		return $this->render('upload', ['model' => $model]);
    }
}
