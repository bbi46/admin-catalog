<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use app\modules\admin\models\Catalog;
use app\modules\admin\models\Settings;
use yii\web\Response;

class SettingsController extends Controller {
	public function actionIndex() {
		
		$settingsModel = new Settings;
		$catalogColumns = $settingsModel->getCatalogFields();
		
		$catalogModel = new Catalog;
		$catalogAttributes = $catalogModel->attributeLabels();
		
		return $this->render('index', [
			'catalog_columns' => $catalogColumns,
			'catalog_attributes' => $catalogAttributes
		]);
	}
	
	public function actionPerform() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		if(Yii::$app->request->post()) {
			$columnId = Yii::$app->request->post('column_id');
			$settings = new Settings;
			$settingExists = $settings->settingExists('show_product_column', $columnId);
			
			$status = '';
			if($settingExists) { 
				$status = 'yes';
				$setting = Settings::findOne($settingExists->id);
				$setting->delete();
			} else {
				$status = 'no';
				$setting = new Settings;
				$setting->user_id = Yii::$app->user->id;
				$setting->setting_name = 'show_product_column';
				$setting->setting_value = $columnId;
				$setting->save();
			}
			
			$responseArr = ['column' => $columnId, 'status' => $status];
			
			return json_encode($responseArr);
		}
	}
}