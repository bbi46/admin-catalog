<?php

namespace app\modules\admin\models;

use Yii;
use \yii\helpers\ArrayHelper;
use app\modules\admin\models\ProductType;

/**
 * This is the model class for table "catalog".
 *
 * @property int $product_id
 * @property int $product_type
 * @property string $name
 * @property string $sku
 * @property string|null $image
 * @property int $on_warehouse
 */
class Catalog extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_type', 'name', 'sku'], 'required'],
            [['product_type', 'on_warehouse'], 'integer'],
            [['name', 'sku'], 'string', 'max' => 64],
            [['imageFile'], 'file', 'extensions' => 'png, jpg'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'ID товара',
            'product_type' => 'Тип товара',
            'name' => 'Название',
            'sku' => 'Sku',
            'image' => 'Изображение',
            'on_warehouse' => 'Кол-во на складе',
        ];
    }
    public static function all($id = false){
  		$all = ProductType::find()->all();
  			if($all){
  			     return ArrayHelper::map($all, 'id', 'type_name');
        }
		    else
  			   return null;
    }

    public function getProducttype() {
      return $this->hasOne(ProductType::className(), ['product_type' => 'id']);
    }
}
