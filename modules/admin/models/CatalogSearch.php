<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Catalog;

/**
 * CatalogSearch represents the model behind the search form of `app\modules\admin\models\Catalog`.
 */
class CatalogSearch extends Catalog
{
    public $search_string;
	/**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'product_type', 'on_warehouse'], 'integer'],
            [['name', 'sku', 'image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Catalog::find();
        $pagination = ['pageSize' => 5];

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pagination
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        /*$query->andFilterWhere([
            'product_id' => $this->product_id,
            'product_type' => $this->product_type,
            'on_warehouse' => $this->on_warehouse,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'sku', $this->sku])
            ->andFilterWhere(['like', 'image', $this->image]);*/
			
		$query->andFilterWhere ( [ 'or' ,
            [ 'like' , 'name' , $this->name ],
            [ 'like' , 'sku' , $this->name ],
        ] );

        return $dataProvider;
    }
}
