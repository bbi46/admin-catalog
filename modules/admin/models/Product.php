<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property int $provider_id
 * @property int $manufacturer_id
 * @property int $ean
 * @property int $count
 * @property float $price
 */
class Product extends \yii\db\ActiveRecord
{
    public $priceFile;
	
	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'provider_id', 'manufacturer_id', 'ean', 'count', 'price'], 'required'],
            [['provider_id', 'manufacturer_id', 'ean', 'count'], 'integer'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 40],
			[['imageFile'], 'file', 'extensions' => 'xlsx']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'provider_id' => 'Provider ID',
            'manufacturer_id' => 'Manufacturer ID',
            'ean' => 'Ean',
            'count' => 'Count',
            'price' => 'Price',
        ];
    }
}
