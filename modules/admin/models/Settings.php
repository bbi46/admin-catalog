<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property int $user_id
 * @property string $setting_name
 * @property string $setting_value
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'setting_name', 'setting_value'], 'required'],
            [['user_id'], 'integer'],
            [['setting_name'], 'string', 'max' => 46],
            [['setting_value'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID Пользователя',
            'setting_name' => 'Название опции',
            'setting_value' => 'Значение настройки',
        ];
    }
	
	public function getCatalogFields() {
		$allTableFields = (new \yii\db\Query())
			->select('*')
			->from('INFORMATION_SCHEMA.COLUMNS')
			->where('TABLE_NAME=:catalogTbl',[':catalogTbl'=>'catalog'])
			->all();
			
			return $allTableFields;
	}
	
	public function settingExists($setting_name, $setting_value) {
		$settingExists = Settings::find()->where('user_id=:curuser', [
				':curuser' => Yii::$app->user->id
			])
			->andWhere('setting_name=:show_prod_clmn', [':show_prod_clmn' => $setting_name])
			->andWhere('setting_value=:clmn_id', [':clmn_id' => $setting_value])
			->one();
			
		return $settingExists ?? false;
	}
}
