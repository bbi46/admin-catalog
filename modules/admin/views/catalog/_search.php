<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\CatalogSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<table class="table table-striped"><thead>
<tr id="w1-filters" class="filters">
<td><input type="text" class="form-control" name="CatalogSearch[name]"></td>
</tr>
</thead>
</table>
<div class="catalog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
    ]); ?>
	

    <?//= $form->field($model, 'search_string') ?>

    <div class="form-group">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
        <?//= Html::resetButton('Сброс', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
