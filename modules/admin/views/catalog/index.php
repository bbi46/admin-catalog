<?php

use yii\bootstrap\Button;
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\models\ProductType;
use app\modules\admin\models\Settings;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\CatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/catalog.js',
  ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="catalog-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <h2>Поиск</h2>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
	
	<p>
        <?= Html::a('Добавить товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p>
        <?= Html::a(
            'Удалить выбранные',
            ['deleteselected'],
            [
              'id' => 'deleteselected',
              'class' => 'btn btn-danger',
              'style' => 'float: right; position: relative; bottom: 10px;',
            ]
          )
        ?>
    </p>
	
	<?php 
		$settingsModel = new Settings;
	?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'product_id',
				'visible' => $settingsModel->settingExists('show_product_column', 'product_id') ? true : false,
			],
            [
              'attribute' => 'product_type',
			  'visible' => $settingsModel->settingExists('show_product_column', 'product_type') ? true : false,
              'format' => 'raw',
              'value' => function($data) {
                return ProductType::find()->where('id=:pt', [':pt' => $data->product_type])->one()->type_name;
              }
            ],
            [
				'attribute' => 'name',
				'visible' => $settingsModel->settingExists('show_product_column', 'name') ? true : false,
			],
			[
				'attribute' => 'sku',
				'visible' => $settingsModel->settingExists('show_product_column', 'sku') ? true : false,
			],
            [
              'attribute' => 'image',
			  'visible' => $settingsModel->settingExists('show_product_column', 'image') ? true : false,
              'format' => 'raw',
              'value' => function($data) {
                return Html::img('@web/uploads/' . $data->image, ['width' => '100px']);
              }
            ],
            [
				'attribute' => 'on_warehouse',
				'visible' => $settingsModel->settingExists('show_product_column', 'on_warehouse') ? true : false,
			],
            [
              'class' => 'yii\grid\CheckboxColumn', 'checkboxOptions' => function($model) {
                return ['value' => $model->product_id];
              },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
