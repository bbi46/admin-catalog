<h2>Меню админа</h2>
<?php 
use yii\helpers\Html;
?>
<p><?= Html::a(Html::encode('Настройки'), '/admin/settings/index') ?></p>
<p><?= Html::a(Html::encode('Весь каталог'), '/admin/catalog/index') ?></p>
<p><?= Html::a(Html::encode('Добавить товар'), '/admin/catalog/create') ?></p>
