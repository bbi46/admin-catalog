<h1>Настройки</h1>
<h2>Колонки в таблице товаров</h2>

<?php

use app\modules\admin\models\Catalog;
use app\modules\admin\models\Settings;

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/settings.js', 
	['depends' => [\yii\web\JqueryAsset::className()]]
);
	$settings = new Settings;

	for($i = 0; $i < count($catalog_columns); $i++) :
	$columnName = $catalog_columns[$i]['COLUMN_NAME'];
	$columnLabel = $catalog_attributes[$columnName];
	$setting_exists = $settings->settingExists('show_product_column', $columnName);
?>
	<input id="<?= $columnName ?>" type="checkbox" <?= $setting_exists ? 'checked' : ''?>/>
	<?=  $columnLabel . ' ' ?>
	<?php endfor; ?>
	
	<?php 
	
	?>
