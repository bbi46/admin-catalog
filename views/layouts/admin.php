<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            !Yii::$app->user->isGuest ? (['label' => 'Админ', 'url' => ['/admin/main/index']]) : '',
            ['label' => 'Домой', 'url' => ['/site/index']],
            ['label' => 'О нас', 'url' => ['/site/about']],
            Yii::$app->user->isGuest ? (
				['label' => 'Регистрация', 'url' => ['/site/signup']]
				. ['label' => 'Вход', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Выход (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => ['label' => 'Главная', 'url' => '/site/index'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
		<aside class="col-lg-4">
			<p><?= Html::a(Html::encode('Настройки'), '/admin/settings/index') ?></p>
			<p><?= Html::a(Html::encode('Весь каталог'), '/admin/catalog/index') ?></p>
			<p><?= Html::a(Html::encode('Добавить товар'), '/admin/catalog/create') ?></p>
			<p><?= Html::a(Html::encode('Загрузить прайс'), '/admin/pricelist/upload') ?></p>
		</aside>
        <div class="col-lg-8"><?= $content ?></div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Каталог товаров <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>