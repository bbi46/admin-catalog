<?php

/* @var $this yii\web\View */

$this->title = 'Каталог товаров';
use app\modules\admin\models\Catalog;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Сайт с каталогом товаров</h1>
    </div>
	
	<?php if (Yii::$app->session->hasFlash('registered')): ?>
		<div class="alert alert-success alert-dismissable">
			 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			 <h4><i class="icon fa fa-check"></i>Вы успешно зарегистрировались!</h4>
			 <?= Yii::$app->session->getFlash('success') ?>
		</div>
	<?php endif; ?>

    <div class="body-content">

        <div class="row">
            
        </div>

    </div>
</div>
