$(document).ready(function() {
  let checkbox = $('input[type=checkbox]');
  let checkedFields = $('input[type=checkbox]:checked');
  const deleteSelectedHref = $('#deleteselected').attr('href');
  
  const setDeletionIds = () => {
	let selectedCheckboxesLength = $('input[type=checkbox]:checked:not(.select-on-check-all)').length;

    var ids = '';
      for(let i = 0; i < selectedCheckboxesLength; i++) {
        let checkedVal = $(`input[type=checkbox]:checked:not(.select-on-check-all):eq(${i})`).val();
        ids += checkedVal;

        if(i !== selectedCheckboxesLength-1) {
          ids += '-';
        }
      }
    $('#deleteselected').attr('href', deleteSelectedHref + "?ids=" + ids);  
  };
  
  setDeletionIds();

  checkbox.change(function() {
    setDeletionIds();
  });
}
);
